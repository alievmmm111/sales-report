SELECT
  calendar_week_number,
  time_id,
  day_name,
  sales,
  SUM(sales) OVER (
    ORDER BY
      time_id ROWS UNBOUNDED PRECEDING
  ) AS CUM_SUM,
  CASE
    WHEN day_name = 'Monday' THEN AVG(sales) OVER (
      ORDER BY
        time_id ROWS BETWEEN 2 FOLLOWING
        AND 1 FOLLOWING
    )
    WHEN day_name = 'Friday' THEN AVG(sales) OVER (
      ORDER BY
        time_id ROWS BETWEEN 1 PRECEDING
        AND 1 FOLLOWING
    )
    ELSE AVG(sales) OVER (
      ORDER BY
        time_id ROWS BETWEEN 1 PRECEDING
        AND 1 FOLLOWING
    )
  END AS CENTERED_3_DAY_AVG
FROM
  SalesData
WHERE
  calendar_week_number IN (49, 50, 51)
  AND YEAR (time_id) = 1999;